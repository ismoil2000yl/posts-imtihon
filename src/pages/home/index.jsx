import React from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { signOut } from 'store/auth'

const index = () => {

  const dispatch = useDispatch()
  const navigate = useNavigate()

  const handleLogout = () => {
    dispatch(signOut())
    navigate("/auth/sign-in")
  }

  return (
    <div>
      <h1>Home page</h1>
      <button 
        className='btn btn-outline-danger my-3'
        onClick={handleLogout}
      >
        Log-out
      </button>
    </div>
  )
}

export default index