import { useMutation, useQuery, useQueryClient } from "@tanstack/react-query";
import { Button, Modal, Table } from "antd";
import axios from "axios";
import { get } from "lodash";
import React, { useState } from "react";
import { useSelector } from "react-redux";
import Form from "./components/form";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { Switch } from 'antd';

const index = () => {
  const [modalData, setModalData] = useState({
    isOpen: false,
    item: null,
  });
  const queryClient = useQueryClient();

  const { mutate: deleteHandler } = useMutation({
    mutationFn: (id) => {
      axios.delete(`http://api.test.uz/api/v1/admin/posts?_l=uz/${id}`, {
        headers: {
          Authorization: "Bearer " + token,
        },
      });
    },
    onSuccess: () => {
      queryClient.invalidateQueries({ queryKey: "banner" });
    },
  });

  const { token } = useSelector((state) => get(state, "auth"));
  const getPosts = () => {
    return axios.get(`http://api.test.uz/api/v1/admin/posts?_l=uz&_t=${new Date().getTime()}`, {
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
        "Accept": "application/json",
      },
    });
  };

  const { data, isLoading } = useQuery({
    queryKey: ["posts"],
    queryFn: getPosts,
  });

  const columns = [
    {
      title: "Title",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "description",
    },
    {
      title: "Content",
      dataIndex: "content",
      key: "content",
    },
    {
      title: "Status",
      dataIndex: "status",
      key: "status",
      render: (_, row)=>{
        return(
          <Switch defaultChecked onChange={()=>{}} />
        )
      }
    },
    {
      title: "Action",
      dataIndex: "type",
      key: "type",
      render: (_, row) => {
        return (
          <div className="flex gap-5">
            <DeleteOutlined
              className="text-red-500 cursor-pointer text-lg"
              onClick={() => deleteHandler(get(row, "id"))}
            />
            <EditOutlined
              className="text-blue-500 cursor-pointer text-lg"
              onClick={() => setModalData({ isOpen: true, item: row })}
            />
          </div>
        );
      },
    },
  ];

  return (
    <div>
      <div className="flex justify-end">
        <Button
          className="mb-2"
          type="primary"
          onClick={() =>
            setModalData({
              isOpen: true,
              item: null,
            })
          }
        >
          Add posts
        </Button>

        <Form {...{ modalData, setModalData }} />
      </div>
      <Table
        dataSource={get(data, "data.data")}
        loading={isLoading}
        columns={columns}
        className="p-2"
      />
      ;
    </div>
  );
};

export default index;
