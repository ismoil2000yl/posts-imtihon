import { useMutation, useQueryClient } from "@tanstack/react-query";
import { Button, Modal } from "antd";
import axios from "axios";
import { Fields } from "components";
import { Field, Form, Formik } from "formik";
import { get } from "lodash";
import React from "react";
import { useSelector } from "react-redux";
import { Switch } from 'antd';

const form = ({ modalData, setModalData }) => {
  const { token } = useSelector((state) => get(state, "auth"));
  const queryClient = useQueryClient();

  const postData = (values) => {
    return axios[modalData.item ? "put" : "post"](
      `http://api.test.uz/api/v1/admin/posts?_l=uz${get(modalData, "item") ? `/${get(modalData, "item.id")}` : ''}`,
      values,
      {
        headers: {
          Authorization: "Bearer " + token,
        },
      }
    );
  };

  // const postData = (values)=>{
  //   console.log(values)
  //   return axios.post("http://api.test.uz/api/v1/admin/posts?_l=uz",values,
  //   {
  //     headers:{
  //               Authorization: "Bearer " + token,
  //               "Content-Type": "application/json",
  //               "Accept": "application/json",
  //             },
  //   }
  //   )
  // }

  const { mutate } = useMutation({
    mutationFn: (values) => postData(values),
    onSuccess: () => {
      setModalData({ isOpen: false, item: null });
      queryClient.invalidateQueries({ queryKey: "posts" });
    },
    onError: (error) => {},
  });

  return (
    <Modal
      title="Post qo'shish"
      open={modalData.isOpen}
      footer={false}
      onCancel={() => setModalData({ isOpen: false, item: null })}
    >
      <Formik
        initialValues={{
          title: get(modalData, "item.title", ""),
          description: get(modalData, "item.description", ""),
          content: get(modalData, "item.content", ""),
        }}
        onSubmit={(values, { resetForm }) => {
          mutate(values);
          resetForm();
        }}
      >
        {({ values, handleSubmit }) => {
          return (
            <Form>
              <Field 
                name="title" 
                label="Title" 
                component={Fields.Input} 
              />
              <Field
                name="description"
                label="Description"
                component={Fields.Input}
              />
              <Field
                name="content"
                label="Content"
                component={Fields.TextArea}
              />
              <Switch defaultChecked onChange={()=>{}} />


              <Button type="primary" onClick={handleSubmit}>
                Submit
              </Button>
            </Form>
          );
        }}
      </Formik>
    </Modal>
  );
};

export default form;
