import { useMutation } from "@tanstack/react-query";
import { Button } from "antd";
import axios from "axios";
import { Fields } from "components";
import { Field, Form, Formik } from "formik";
import { get } from "lodash";
import React from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { signIn } from "store/auth";
import { SignUserAvatar } from 'assets/images/png'


const index = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const mutation = useMutation({
    mutationFn: (newTodo) => {
      return axios.post(
        "http://api.test.uz/api/v1/admin/user/sign-in",
        newTodo
      );
    },
    onSuccess: (data) => {
      navigate("/");
      dispatch(signIn(get(data, "data.data")));
    },
  });
  
 

  return (
    <div className="w-screen h-screen flex items-center justify-center bg-sky-400">
      <div className="w-1/2 shadow-md p-10 rounded-md bg-white">
        <Formik
          initialValues={{
            username: "",
            password: "",
            phone: "",
            status: 1,
          }}
          onSubmit={(data) => {
            console.log(data);
          }}
        >
          {({ values }) => {
            return (
              <Form>
                <div className='text-center'>
                  <img src={SignUserAvatar} className="sign-user-avatar"/>
                  <h3 className='my-2'>Login</h3>
                </div>
                <Field name="phone" label="Phone" component={Fields.Input} />
                <Field
                  name="username"
                  label="User name"
                  component={Fields.Input}
                />

                <Field
                  name="password"
                  label="Password"
                  component={Fields.Input}
                />
                <Button 
                  className='col-md-12' 
                  size='large' 
                  type='primary' 
                  onClick={() => mutation.mutate(values)}
                >
                  Login
                </Button>
                <button 
                  className='btn btn-outline-success my-2 col-md-12' 
                  onClick={() => navigate("/auth/registration")}
                >
                  Registration
                </button>
              </Form>
            );
          }}
        </Formik>
      </div>
    </div>
  );
};

export default index;
