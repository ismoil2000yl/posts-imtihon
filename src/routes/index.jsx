import { lazy } from "react";

const Home = lazy(() => import("pages/home"));
const Posts = lazy(() => import("pages/posts"));
const Registration = lazy(() => import("pages/auth/registration"));
const SignIn = lazy(() => import("pages/auth/signIn"));
const Login = lazy(() => import("pages/auth/login"));

const authRoutes = [
  {
    path: "/auth/login",
    element: <Login />,
  },
  {
    path: "/auth/registration",
    element: <Registration />,
  },
  {
    path: "/auth/sign-in",
    element: <SignIn />,
  },
];

const privateRoutes = [
  {
    path: "/",
    element: <Home />,
  },
  {
    path: '/posts',
    element: <Posts />
  }
];

export { authRoutes, privateRoutes };
